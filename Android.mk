# verify it isn't a simulator build
ifneq (,$(filter $(QCOM_BOARD_PLATFORMS),$(TARGET_BOARD_PLATFORM)))

# Saving current directory since '$(call my-dir)'
# is changing while iterating all makefiles.
CIMAX_PATH := $(call my-dir)

include $(CIMAX_PATH)/CIMAX_SPI_Driver/Android.mk

endif
